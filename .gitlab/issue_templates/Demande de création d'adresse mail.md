# Demande de création d'une adresse mail `@agepoly.ch`
<!-- Ceci est un commentaire, il ne sera pas affiché lors de la demande -->
## À remplir par le réquerant
<!-- Tous les champs ci-dessous sont obligatoires, sauf indication -->
- Type:  <!-- Type: Alias (Vers un utilisateur), Groupe, Redirection (Vers un groupe) -->
- Motif:  <!-- Facultatif pour les alias -->
- Adresse de souhaité: `@agepoly.ch`  <!-- e.g. `equipe.animation@agepoly.ch` -->
- Destinataires:
    -   <!-- une adresse par ligne -->
- Description: 
- Ouverte à des externes: <!-- i.e. Cette adresse peut recevoir des mails provenant des adresses autres que `@agepoly.ch` -->

<!-- Si vous souhaitez garder ceci confidentiel, prière de cocher la case “This issue is confidential...” en bas de cette boîte -->

__Merci de votre demande, elle sera traité dans les plus bref délais. Thanks for flying AGEPoly__

<!-- Fin des informations du requérant. Prière de ne pas toucher aux lignes ci-dessous. -->
/label ~"@à-faire"
/label ~"priority/3:required"
/label ~"type:tâche"
/assign @responsable\_informatique\_agepoly